using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Samarnggoon.GameDev4.UIToolkit
{
    public class MainMenuEventHandler12 : MonoBehaviour
    {
        public UIDocument _uiDocument;

        public VisualElement _startButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _startButton = _uiDocument.rootVisualElement.Query<Button>("12-button");
        }

        private void OnEnable()
        {
            _startButton.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }

        private void OnStartButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("Unit 5.2");
        }

        private void OnDisable()
        {
            _startButton.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }
    }
}